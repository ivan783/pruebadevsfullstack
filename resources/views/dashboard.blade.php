<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Inicio') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <h3 class="text-2xl font-bold text-gray-800 mb-4">Prueba Devs Full Stack</h3>
                <p class="text-gray-600">Tenemos un proyecto el cual el desarrollador no pudo llevar a cabo, porque se fue de vacaciones!!!</p>
                <p class="text-gray-600">El examen consta de varias secciones que evaluarán diferentes aspectos de la programación web, como HTML, CSS, JavaScript, y frameworks como Laravel.</p>
                <p class="text-gray-600">Asegúrate de leer cuidadosamente cada pregunta y proporcionar la solución adecuada. ¡Buena suerte!</p>
                <p class="text-gray-600">Se solicita realizar un ABM/CRUD (Alta, Baja y Modificacion) de registros de Usuarios, que se compone con los siguientes atributos:</p>
                <ol class="list-decimal ml-8 text-gray-600">
                    <li>Nombre</li>
                    <li>Apellido</li>
                    <li>Correo</li>
                    <li>Nro de telefono</li>
                    <li>Contraseña</li>
                </ol>
                <p class="text-gray-600">Estos usuarios deberán poder loguearse para administrar los datos de este administrador de usuarios (ABM/CRUD)</p>
                <p class="text-gray-600">Utilizar las Tecnologías Tailwind, PHP Laravel-Livewire, Base de datos relaciones.</p>
            </div>
        </div>
        </div>
        </div>
    </div>
    </div>
</x-app-layout>
