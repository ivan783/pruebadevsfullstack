<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Crear Usuario') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div class="max-w-lg mx-auto bg-white shadow-md rounded-lg overflow-hidden">
                    <div class="px-6 py-4">
                        <h2 class="text-2xl font-semibold text-gray-800 mb-2">Crear Usuario</h2>
                        <form method="POST" action="{{ route('users.store') }}">
                            @csrf
                            <div class="mb-4">
                                <label class="block text-gray-700 text-sm font-bold mb-2">Nombre</label>
                                <input type="text" name="name" class="form-input mt-1 block w-full" placeholder="Introduce tu nombre" required>
                            </div>
                            <div class="mb-4">
                                <label class="block text-gray-700 text-sm font-bold mb-2">Apellido</label>
                                <input type="text" name="last_name" class="form-input mt-1 block w-full" placeholder="Introduce tu apellido" required>
                            </div>
                            <div class="mb-4">
                                <label class="block text-gray-700 text-sm font-bold mb-2">Email</label>
                                <input type="email" name="email" class="form-input mt-1 block w-full" placeholder="Introduce tu email" required>
                            </div>
                            <div class="mb-4">
                                <label class="block text-gray-700 text-sm font-bold mb-2">Teléfono</label>
                                <input type="tel" name="phone" class="form-input mt-1 block w-full" placeholder="Introduce tu teléfono" required>
                            </div>
                            <div class="mb-4">
                                <label class="block text-gray-700 text-sm font-bold mb-2">Contraseña</label>
                                <input type="password" name="password" class="form-input mt-1 block w-full" placeholder="Introduce tu contraseña" required>
                            </div>
                            <div class="flex justify-between">
                                <button type="submit" class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Guardar</button>
                                <a href="{{route('users.index')}}" class="text-white bg-red-500 hover:bg-red-700 font-bold py-2 px-4 rounded inline-block">Cancelar</a>
                            </div>
                        </form>
                    </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</x-app-layout>
